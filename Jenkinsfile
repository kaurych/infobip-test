import groovy.json.JsonBuilder
import groovy.json.JsonOutput

def containerGateway
def containerProcessor
def containerQueue
String verifyResponse
def rbinUrl

try {
    // Define global options
    currentBuild.result = "SUCCESS"
    env.DOCKER_HOST = "192.168.36.1:4243"
    
    stage ('Prepare: Check up environment') {
        node ('master') {
            sh ('export');
        }
    }
    
    stage ('Prepare: Create request bin') {
        node ('master') {
            def rbin = httpRequest( consoleLogResponseBody: true, 
                                httpMode: 'POST',
                                url: "http://requestb.in/api/v1/bins").getContent()
            def slrp = new groovy.json.JsonSlurper()
            rbinUrl = slrp.parseText(rbin).name
            println rbinUrl
        }
    }
   
    stage ('Prepare: Checkout git repository') {
        // Define git repo options for single branch build
        /* -----------------------------------This block saved for development purpose-------------------------------
        def gitBranch = "develop";
        def gitRepoURL = "git@gitlab.com:nikolyabb/infobip-test.git"
        def gitRepoCreds = 'a45e82ae-0b61-408f-9fc1-47f5d876a8b3'*/
        
        node ('master') {
            dir ('vcs') {
                //git branch: "${gitBranch}", url: "${gitRepoURL}", credentialsId: "${gitRepoCreds}"
                checkout scm
            }
        }
    }
   
    stage ('Build: Build and deploy Messaging-gateway to Artifactory') {
        node ('master') {
            // Define Artifactory server parameters
            def artSrvCreds = '4e4d9867-792b-42a0-89c1-046f33a54bb0'
            def artSrv = Artifactory.newServer url: 'http://192.168.36.1:3080/artifactory', credentialsId: "${artSrvCreds}"
            
            // Define build parameters
            def rtMaven = Artifactory.newMavenBuild()
            rtMaven.resolver server: artSrv, releaseRepo: 'libs-release', snapshotRepo: 'libs-snapshot'
            rtMaven.deployer server: artSrv, releaseRepo: 'libs-release-local', snapshotRepo: 'libs-snapshot-local'
            rtMaven.deployer.deployArtifacts = true
            rtMaven.tool = 'Maven3'
            rtMaven.opts = '-Xms128m -Xmx512m'
            
            dir ('vcs') {
                def buildInfo = rtMaven.run pom: 'pom.xml', goals: 'clean install'
                echo '[MAVEN] Build completed'
                
                artSrv.publishBuildInfo buildInfo
                echo '[MAVEN] Build info published'
            }
        }
    }
    
    def testIP
    stage ('Build: Run app in Docker for testing') {
        docker.withTool("docker") {
            withDockerServer([uri: "${env.DOCKER_HOST}"]) { 

                // Get current conatiner state: if exist - hash, if not - null
                containerGateway = sh(returnStdout:true, script: "docker ps -a --format '{{ .ID}}' --filter 'name=ib-gw'").trim() 
                containerProcessor = sh(returnStdout:true, script: "docker ps -a --format '{{ .ID}}' --filter 'name=ib-proc'").trim()
                containerQueue = sh(returnStdout:true, script: "docker ps -a --format '{{ .ID}}' --filter 'name=ib-rabbitmq'").trim()
                
                //Check for none containers with predefined names is running, and kill any if exist
                if (containerGateway.length() > 0 ) { 
                    sh(returnStdout:true, script: "docker kill ${containerGateway}") 
                    echo "Container ${containerGateway} killed"
                }
                if (containerProcessor.length() > 0 ) { 
                    sh(returnStdout:true, script: "docker kill ${containerProcessor}") 
                    echo "Container ${containerProcessor} killed"
                }
                if (containerQueue.length() > 0 ) { 
                    sh(returnStdout:true, script: "docker kill ${containerQueue}") 
                    echo "Container ${containerQueue} killed"
                }

                dir ('vcs') {
                    echo "[DOCKER] Run RabbitMQ container"
                    containerQueue = sh(returnStdout:true, script:'docker run --rm --name ib-rabbitmq -it -d rabbitmq').trim()
                    testIP = sh(returnStdout:true, script: "docker inspect --format '{{ .NetworkSettings.IPAddress }}' ${containerQueue}").trim()
                    echo "[DOCKER] Build Gateway app container"
                    sh 'docker build -t infobip/msg-gw -f Dockerfile.gw .'
                    echo "[DOCKER] Run Gateway app container"
                    containerGateway = sh(returnStdout:true, script:'docker run --rm --name ib-gw --net="container:ib-rabbitmq" -it -d infobip/msg-gw').trim()
                    echo "[DOCKER] Build Processor app container"
                    sh 'docker build -t infobip/msg-proc -f Dockerfile.proc .'
                    echo "[DOCKER] Run Processor app container"
                    containerProcessor = sh(returnStdout:true, script:'docker run --rm --name ib-proc --net="container:ib-rabbitmq" -it -d infobip/msg-proc').trim()
                }
            }
        }
    }
    stage ('Verify: Send test messages to Gateway') {
        node ('master') {
            json_v_200 = JsonOutput.toJson([
                messageId: 3,
                timestamp: 3234,
                protocolVersion: '2.0.0',
                payload: [
                    mMX: 1234,
                    mPermGen: 5678,
                    mOldGen: 22222,
                    mYoungGen: 333333]
            ])
            json_v_101 = JsonOutput.toJson([
                messageId: 2,
                timestamp: 2234,
                protocolVersion: '1.0.1',
                messageData: [
                    mMX: 1234,
                    mPermGen: 5678,
                    mOldGen: 22222]
            ])
            json_v_100 = JsonOutput.toJson([
                messageId: 1,
                timestamp: 1234,
                protocolVersion: '1.0.0',
                messageData: [
                    mMX: 1234,
                    mPermGen: 1234]
            ])
        
            testReq = [json_v_100, json_v_101, json_v_200]
            String testUrl = "http://${testIP}:8080/message"
            sleep 20 // Wait for Tomcat going online
            
            for (req in testReq) {
                echo "Sending to ${testUrl} message: ${req}"
                response = httpRequest  consoleLogResponseBody: true, 
                                        contentType: 'APPLICATION_JSON',
                                        httpMode: 'POST',
                                        requestBody: req,
                                        url: "${testUrl}"
                println response
            }
        }
    }
    
    stage ('Verify: Collect messages from Processor') {
        node ('master') {
            verifyResponse = httpRequest(url: "http://${env.DOCKER_HOST}/containers/${containerProcessor}/logs?stdout=1").content
            echo verifyResponse
        }
    }    
    stage ('Remove docker containers') {
        docker.withTool("docker") {
            withDockerServer([uri: "${env.DOCKER_HOST}"]) { 
                echo "[DOCKER] Stop test containers"
                sh returnStdout:true, script:"docker kill ${containerQueue} ${containerGateway} ${containerProcessor}"
            }
        }
    }
    stage ('Send JSON') {
        node ('master') {
            jsonBuildStat = JsonOutput.toJson([
                buildInfo: [
                    buildTag: "${env.BUILD_TAG}",
                    buildDuration: "${currentBuild.duration}", 
                    buildUrl: "${currentBuild.absoluteUrl}",
                    buildResult: "${currentBuild.result}",
                    buildError: null],
                deployInfo: [
                    deployServer: "${env.DOCKER_HOST}",
                    deployQueueContainer: "${containerQueue}",
                    deployProcessorContainer: "${containerProcessor}",
                    deployGatewayConatiner: "${containerGateway}"],
                verificationInfo: [
                    verificationRawOutput: "${verifyResponse}"]
            ])
                
            response = httpRequest  consoleLogResponseBody: true, 
                                    contentType: 'APPLICATION_JSON',
                                    httpMode: 'POST',
                                    requestBody: jsonBuildStat,
                                    url: "https://requestb.in/${rbinUrl}"
            println response
            echo "Find more info on https://requestb.in/${rbinUrl}?inspect"
        }
   }
}
catch (err) {
    currentBuild.result = "FAILURE"
    
    stage ('FAILURE: Send JSON') {
       node ('master') {
            println(err)
            jsonBuildStat = JsonOutput.toJson([
                buildInfo: [
                    buildTag: "${env.BUILD_TAG}",
                    buildDuration: "${currentBuild.duration}", 
                    buildUrl: "${currentBuild.absoluteUrl}",
                    buildResult: "${currentBuild.result}",
                    buildError: "${err}"],
                deployInfo: [
                    deployServer: "${env.DOCKER_HOST}",
                    deployQueueContainer: "${containerQueue}",
                    deployProcessorContainer: "${containerProcessor}",
                    deployGatewayConatiner: "${containerGateway}"],
                verificationInfo: [
                    verificationRawOutput: "${verifyResponse}"]
            ])
                
            response = httpRequest  consoleLogResponseBody: true, 
                                    contentType: 'APPLICATION_JSON',
                                    httpMode: 'POST',
                                    requestBody: jsonBuildStat,
                                    url: "https://requestb.in/${rbinUrl}"
            println response
            echo "Find more info on https://requestb.in/${rbinUrl}?inspect"
        }
    }
}
